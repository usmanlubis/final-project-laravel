<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hub extends Model
{
    //
    protected $table = "hub";
    protected $fillable = ["nm_area", "jenis_koneksi","alamat","area_id"];
}
