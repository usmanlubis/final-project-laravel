<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Hub;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::select(
            "customer.id", 
            "customer.nm_customer",
            "customer.no_telp",
            "customer.jenis_koneksi_customer",
            "customer.harga_sewa",
            "customer.alamat_customer",
            "customer.hub_id",
            "hub.nm_hub"
        )
        ->leftJoin("hub", "customer.hub_id", "=", "hub.id")
        ->get();
        //
        //$hub = Hub::all();

        return view('customer.index',compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $hub = Hub::pluck('nm_hub', 'id');

        return view('customer.create',compact('hub'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
            'nm_customer' => 'required|max:255',
            'jenis_koneksi_customer' => 'required|max:255',
            'no_telp' => 'required',
            'harga_sewa' => 'required',
            'hub_id' => 'required',
            'alamat_customer' => 'required',
            ],
            [
            'nm_customer.required' => 'Nama area belum diisi',
            'jenis_koneksi_customer.required' => 'Jenis Koneksi belum diisi',
            'no_telp.required' => 'No Telp belum diisi',
            'harga_sewa.required' => 'Harga belum diisi',
            'hub_id.required' => 'Pilih Hub',
            'alamat_customer.required' => 'Alamat belum diisi',
            ]
        );

        $customer = new Customer;
 
        $customer->nm_customer = $request->nm_customer;
        $customer->jenis_koneksi_customer = $request->jenis_koneksi_customer;
        $customer->no_telp = $request->no_telp;
        $customer->harga_sewa = $request->harga_sewa;
        $customer->hub_id = $request->hub_id;
        $customer->alamat_customer = $request->alamat_customer;
 
        $customer->save();

        return redirect ('/customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $customer = Customer::find($id);
        $hub = Hub::pluck('nm_hub', 'id');

        //dd($area);
        return view('customer.show',compact('hub','customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $customer = Customer::find($id);
        $hub = Hub::pluck('nm_hub', 'id');
        //dd($cast);
        return view('customer.edit',compact('customer','hub'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(
            [
            'nm_customer' => 'required|max:255',
            'jenis_koneksi_customer' => 'required|max:255',
            'no_telp' => 'required',
            'harga_sewa' => 'required',
            'hub_id' => 'required',
            'alamat_customer' => 'required',
            ],
            [
            'nm_customer.required' => 'Nama area belum diisi',
            'jenis_koneksi_customer.required' => 'Jenis Koneksi belum diisi',
            'no_telp.required' => 'No Telp belum diisi',
            'harga_sewa.required' => 'Harga belum diisi',
            'hub_id.required' => 'Pilih Hub',
            'alamat_customer.required' => 'Alamat belum diisi',
            ]
        );

        $customer = Customer::find($id);
 
        $customer->nm_customer = $request->nm_customer;
        $customer->jenis_koneksi_customer = $request->jenis_koneksi_customer;
        $customer->no_telp = $request->no_telp;
        $customer->harga_sewa = $request->harga_sewa;
        $customer->hub_id = $request->hub_id;
        $customer->alamat_customer = $request->alamat_customer;
        
        $customer->save();

        return redirect ('/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $customer = Customer::find($id);
 
        $customer->delete();

        return redirect ('/customer');
    }
}
