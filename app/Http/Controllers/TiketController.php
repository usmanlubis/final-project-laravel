<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tiket;
use App\Customer;

class TiketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tiket = Tiket::select(

            "tiket.id", 
            "tiket.created_at",
            "tiket.keluhan",
            "customer.nm_customer"
        )
        ->leftJoin("customer", "tiket.customer_id", "=", "customer.id")
        ->get();
        //
        //$hub = Hub::all();

        return view('tiket.index',compact('tiket'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $customer = Customer::pluck('nm_customer', 'id');

        return view('tiket.create',compact('customer'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
            'customer_id' => 'required',
            'keluhan' => 'required|max:255'
            ],
            [
            'keluhan.required' => 'Keluhan belum diisi',
            'customer_id.required' => 'Pilih Customer',
            ]
        );

        $tiket = new Tiket;
 
        $tiket->customer_id = $request->customer_id;
        $tiket->keluhan = $request->keluhan;
        $tiket->status = 'OPEN';
 
        $tiket->save();

        return redirect ('/tiket');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tiket = Tiket::find($id);
        $customer = Customer::pluck('nm_customer', 'id');

        //dd($area);
        return view('tiket.show',compact('tiket','customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tiket = Tiket::find($id);
        $customer = Customer::pluck('nm_customer', 'id');
        //dd($cast);
        return view('tiket.edit',compact('tiket','customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
            'customer_id' => 'required',
            'keluhan' => 'required|max:255'
            ],
            [
            'keluhan.required' => 'Keluhan belum diisi',
            'customer_id.required' => 'Pilih Customer',
            ]
        );

        $tiket = Tiket::find($id);
 
        $tiket->customer_id = $request['customer_id'];
        $tiket->keluhan = $request['keluhan'];
        
        
        $tiket->save();

        return redirect ('/tiket');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tiket = Tiket::find($id);
 
        $tiket->delete();

        return redirect ('/tiket');
    }
}
