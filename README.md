## Final Project

## Kelompok 1
- Usman Lubis
- Muhammad Irfan
- Verdy Azr

## Tema Project: Web Pengaduhan layanan Pelanggan Open Net

## ERD
<p><img src="ERD-Final.png"></p>

<p>Link video demo: https://drive.google.com/file/d/1Hu2BiiYtMje0N24FBbNAg0UUDi-bkNRd/view?usp=sharing</p>
<p>Link web : https://final.opennet.sanbercodeapp.com/</p>