<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nm_customer');
            $table->string('no_telp',45);
            $table->string('jenis_koneksi_customer');
            $table->integer('harga_sewa');
            $table->string('alamat_customer');
            $table->unsignedBigInteger('hub_id');
            $table->foreign('hub_id')->references('id')->on('hub');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
