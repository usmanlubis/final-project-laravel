<?php

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/master', function () {
    return view('layout.master');
});

Route::group(['middleware' => ['auth']], function () {
    //dashboard
    Route::get('/dashboard', 'DashboardController@index');

    //crud area
    Route::get('/area/create', 'AreaController@create');
    Route::post('/area', 'AreaController@store');

    Route::get('/area', 'AreaController@index');
    Route::get('/area/{area_id}', 'AreaController@show');

    Route::get('/area/{area_id}/edit', 'AreaController@edit');
    Route::put('/area/{area_id}', 'AreaController@update');

    Route::delete('/area/{area_id}', 'AreaController@destroy');

    //crud hub
    Route::get('/hub/create', 'HubController@create');
    Route::post('/hub', 'HubController@store');

    Route::get('/hub', 'HubController@index');
    Route::get('/hub/{hub_id}', 'HubController@show');

    Route::get('/hub/{hub_id}/edit', 'HubController@edit');
    Route::put('/hub/{hub_id}', 'HubController@update');

    Route::delete('/hub/{hub_id}', 'HubController@destroy');

    //crud customer
    Route::get('/customer/create', 'CustomerController@create');
    Route::post('/customer', 'CustomerController@store');

    Route::get('/customer', 'CustomerController@index');
    Route::get('/customer/{customer_id}', 'CustomerController@show');

    Route::get('/customer/{customer_id}/edit', 'CustomerController@edit');
    Route::put('/customer/{customer_id}', 'CustomerController@update');

    Route::delete('/customer/{customer_id}', 'CustomerController@destroy');

    //crud keluhan
    Route::get('/tiket/create', 'TiketController@create');
    Route::post('/tiket', 'TiketController@store');

    Route::get('/tiket', 'TiketController@index');
    Route::get('/tiket/{tiket_id}', 'TiketController@show');

    Route::get('/tiket/{tiket_id}/edit', 'TiketController@edit');
    Route::put('/tiket/{tiket_id}', 'TiketController@update');

    Route::delete('/tiket/{tiket_id}', 'TiketController@destroy');

    //keluhan aktif
    Route::get('/status_tiket_open', 'StatustiketopenController@index');
    Route::get('/status_tiket_open/{tiket_id}/edit', 'StatustiketopenController@edit');
    Route::put('/status_tiket_open/{tiket_id}', 'StatustiketopenController@update');

    //keluhan terselesaikan
    Route::get('/status_tiket_close', 'StatustiketcloseController@index');
    Route::get('/status_tiket_close/{tiket_id}/edit', 'StatustiketcloseController@edit');
    Route::put('/status_tiket_close/{tiket_id}', 'StatustiketcloseController@update');

    //profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);
});


Auth::routes();
