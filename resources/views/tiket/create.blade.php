@extends('layout.master')
@section('title')
@endsection
@section('content')

<form method="post" action='/tiket'>
    @csrf
    <div class="row">
        <div class="col-xl-12 mx-auto">
            <div class="card border-top border-0 border-4 border-info">
                <div class="card-body">
                    <div class="border p-4 rounded">
                        <div class="card-title d-flex align-items-center">
                            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
                            </div>
                            <h5 class="mb-0 text-info">Tiket</h5>
                        </div>
                        <hr>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Customer</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="customer_id">
                                    <option value=''>Pilih Customer</option>
                                    @foreach ($customer as $key => $value)
                                        <option value="{{ $key }}">                           
                                            {{ $value }} 
                                        </option>
                                    @endforeach    
                                </select>
                                @error('customer_id')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Keluhan</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="keluhan" name='keluhan' rows="3"></textarea>
                                @error('keluhan')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info px-5">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </form>

@endsection