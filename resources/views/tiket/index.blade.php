@extends('layout.master')
@section('title')
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h6 class="mb-0 text-uppercase">Data Keluhan</h6>
        <hr>
        <a href="/tiket/create" class="btn btn-primary mb-1 btn-sm">Tambah Keluhan</a>
        <div class="card">
            <div class="card-body">
                <table class="table mb-0 table-striped">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Tgl Keluhan</th>
                            <th scope="col">Customer</th>
                            <th scope="col">Keluhan</th>
                            <th scope="col" width='20%'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($tiket as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->nm_customer}}</td>
                            <td>{{$item->keluhan}}</td>
                            <td>

                                <form class="mt-2" action="/tiket/{{$item->id}}" method="post">
                                    <a href="/tiket/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                                    <a href="/tiket/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    @csrf
                                    @method('delete')
                                    <input type="submit" value='Delete' class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan='6'>Data Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection