<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
	<link rel="icon" href="assets/images/favicon-32x32.png" type="image/png" />
	<!--plugins-->
	<link href="{{asset('template/assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
	<link href="{{asset('template/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet')}}" />
	<link href="{{asset('template/assets/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
	<!-- loader-->
	<link href="{{asset('template/assets/css/pace.min.css')}}" rel="stylesheet" />
	<script src="{{asset('template/assets/js/pace.min.js')}}"></script>
	<!-- Bootstrap CSS -->
	<link href="{{asset('template/assets/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('template/assets/css/bootstrap-extended.css')}}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<link href="{{asset('template/assets/css/app.css')}}" rel="stylesheet">
	<link href="{{asset('template/assets/css/icons.css')}}" rel="stylesheet">
	<title>Synadmin – Bootstrap5 Admin Template</title>
</head>

<body>
	<!--wrapper-->
	<div class="wrapper">
		<div class="authentication-header"></div>
		<div class="d-flex align-items-center justify-content-center my-5 my-lg-0">
			<div class="container">
				<div class="row row-cols-1 row-cols-lg-2 row-cols-xl-2">
					<div class="col mx-auto">
						<div class="my-4 text-center">
							<img src="assets/images/logo-img.png" width="180" alt="" />
						</div>
						<div class="card">
							<div class="card-body">
								<div class="p-4 rounded">
									<div class="text-center">
										<h3 class="">Sign Up</h3>
										<p>Already have an account? <a href="/login">Sign in here</a>
										</p>
									</div>
									<div class="form-body">
										<form class="row g-3" method="POST" action="{{ route('register') }}">
											@csrf
											<div class="col-sm-12">
												<label for="inputFirstName" class="form-label">Nama</label>
												<input type="text" name="name" class="form-control" id="inputFirstName">
												@error('name')
												<div class="text-danger">{{ $message }}</div>
												@enderror
											</div>
											<div class="col-12">
												<label for="inputEmailAddress" class="form-label">Email Address</label>
												<input type="email" class="form-control" id="email" name='email' placeholder="example@user.com">
												@error('email')
												<div class="text-danger">{{ $message }}</div>
												@enderror
											</div>
											<div class="col-12">
												<label for="inputChoosePassword" class="form-label">Password</label>
												<div class="input-group" id="show_hide_password">
													<input type="password" class="form-control border-end-0" id="password" name='password' placeholder="Enter Password"> <a href="javascript:;" class="input-group-text bg-transparent"><i class='bx bx-hide'></i></a>
												</div>
												@error('password')
												<div class="text-danger">{{ $message }}</div>
												@enderror
											</div>
											<div class="col-12">
												<label for="inputChoosePassword" class="form-label">Confirm Password</label>
												<div class="input-group" id="show_hide_password">
													<input type="password" class="form-control border-end-0" id="password-confirm" name='password_confirmation' placeholder="Enter Password"> <a href="javascript:;" class="input-group-text bg-transparent"><i class='bx bx-hide'></i></a>
												</div>
												@error('password_confirmation')
												<div class="text-danger">{{ $message }}</div>
												@enderror
											</div>
											<div class="col-12">
												<label for="bio" class="form-label">Bio</label>
												<div class="input-group" id="bio">
													<textarea class="form-control" id="bio" name='bio' rows="3"></textarea>
												</div>
												@error('bio')
												<div class="text-danger">{{ $message }}</div>
												@enderror
											</div>
											<div class="col-12">
												<label for="bio" class="form-label">Alamat</label>
												<div class="input-group" id="alamat">
													<textarea class="form-control" id="alamat" name='alamat' rows="3"></textarea>
												</div>
												@error('alamat')
												<div class="text-danger">{{ $message }}</div>
												@enderror
											</div>
											<div class="col-12">
												<div class="d-grid">
													<button type="submit" class="btn btn-primary"><i class='bx bx-user'></i>Sign up</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end row-->
			</div>
		</div>
	</div>
	<!--end wrapper-->
	<!-- Bootstrap JS -->
	<script src="assets/js/bootstrap.bundle.min.js"></script>
	<!--plugins-->
	<script src="{{asset('template/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('template/assets/plugins/simplebar/js/simplebar.min.js')}}"></script>
	<script src="{{asset('template/assets/plugins/metismenu/js/metisMenu.min.js')}}"></script>
	<script src="{{asset('template/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
	<!--Password show & hide js -->
	<script>
		$(document).ready(function() {
			$("#show_hide_password a").on('click', function(event) {
				event.preventDefault();
				if ($('#show_hide_password input').attr("type") == "text") {
					$('#show_hide_password input').attr('type', 'password');
					$('#show_hide_password i').addClass("bx-hide");
					$('#show_hide_password i').removeClass("bx-show");
				} else if ($('#show_hide_password input').attr("type") == "password") {
					$('#show_hide_password input').attr('type', 'text');
					$('#show_hide_password i').removeClass("bx-hide");
					$('#show_hide_password i').addClass("bx-show");
				}
			});
		});
	</script>
	<!--app JS-->
	<script src="{{asset('template/assets/js/app.js')}}"></script>
</body>

</html>