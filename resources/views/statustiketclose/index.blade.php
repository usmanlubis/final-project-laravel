@extends('layout.master')
@section('title')
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h6 class="mb-0 text-uppercase">Keluhan Terselesaikan</h6>
        <hr>
        <!--<a href="/tiket/create" class="btn btn-primary mb-1 btn-sm">Tambah </a>-->
        <div class="card">
            <div class="card-body">
                <table class="table mb-0 table-striped">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Tgl Keluhan</th>
                            <th scope="col">Customer</th>
                            <th scope="col">Keluhan</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($tiket as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->nm_customer}}</td>
                            <td>{{$item->keluhan}}</td>
                            <td>{{$item->status}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan='6'>Data Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection