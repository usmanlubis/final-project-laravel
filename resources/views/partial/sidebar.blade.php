<!--sidebar wrapper -->
<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="{{asset('template/assets/images/logo-icon.png')}}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">OpenNet</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-first-page'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li>
            <a href="/dashboard">
                <div class="parent-icon"><i class='bx bx-home'></i>
                </div>
                <div class="menu-title">Dashboard</div>
            </a>
        </li>
        @auth
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-spa'></i>
                </div>
                <div class="menu-title">Master</div>
            </a>
            <ul>
                <li> <a href="/area"><i class="bx bx-right-arrow-alt"></i>Area</a>
                </li>
                <li> <a href="/hub"><i class="bx bx-right-arrow-alt"></i>Hub</a>
                </li>
                <li> <a href="/customer"><i class="bx bx-right-arrow-alt"></i>Customer</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-spa'></i>
                </div>
                <div class="menu-title">Keluhan</div>
            </a>
            <ul>
                <li> <a href="/tiket"><i class="bx bx-right-arrow-alt"></i>Daftar Keluhan</a>
                </li>
                <li> <a href="/status_tiket_open"><i class="bx bx-right-arrow-alt"></i>Status Keluhan</a>
                </li>
                <li> <a href="/status_tiket_close"><i class="bx bx-right-arrow-alt"></i>Keluhan Terselesaikan</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="/profile">
                <div class="parent-icon"><i class='bx bx-user'></i>
                </div>
                <div class="menu-title">Profile</div>
            </a>
        </li>
        <li>
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <div class="parent-icon"> <i class="bx bx-log-out"></i>
                </div>
                <div class="menu-title">{{ __('Logout') }}</div>

            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
        @endauth

    </ul>
    <!--end navigation-->
</div>
<!--end sidebar wrapper -->