@extends('layout.master')
@section('title')
@endsection
@section('content')

<form method="post" action='/customer/{{$customer->id}}'>
    @csrf
    @method('put')
    <div class="row">
        <div class="col-xl-12 mx-auto">
            <div class="card border-top border-0 border-4 border-info">
                <div class="card-body">
                    <div class="border p-4 rounded">
                        <div class="card-title d-flex align-items-center">
                            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
                            </div>
                            <h5 class="mb-0 text-info">Customer</h5>
                        </div>
                        <hr>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Nama Customer</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name='nm_customer' value="{{$customer->nm_customer}}">
                                @error('nm_customer')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">No Telp</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name='no_telp' value="{{$customer->no_telp}}">
                                @error('no_telp')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Jenis Koneksi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name='jenis_koneksi_customer' value="{{$customer->jenis_koneksi_customer}}">
                                @error('jenis_koneksi_customer')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Harga Sewa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name='harga_sewa' value="{{$customer->harga_sewa}}">
                                @error('harga_sewa')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Hub</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="hub_id">
                                    <option value=''>Pilih Hub</option>
                                    @foreach ($hub as $key => $value)
                                        <option value="{{ $key }}" {{ ( $key == $customer->hub_id) ? "selected" : '' }}>                           
                                            {{ $value }} 
                                        </option>
                                    @endforeach    
                                </select>
                                @error('hub_id')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="alamat_customer" name='alamat_customer' rows="3">{{$customer->alamat_customer}}</textarea>
                                @error('alamat_customer')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info px-5">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </form>

@endsection