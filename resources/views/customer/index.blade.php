@extends('layout.master')
@section('title')
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h6 class="mb-0 text-uppercase">Data Customer</h6>
        <hr>
        <a href="/customer/create" class="btn btn-primary mb-1 btn-sm">Tambah </a>
        <div class="card">
            <div class="card-body">
                <table class="table mb-0 table-striped">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col" >Nama Customer</th>
                            <th scope="col" >No Telp</th>
                            <th scope="col" >Jenis Koneksi</th>
                            <th scope="col" >Harga</th>
                            <th scope="col" >Alamat</th>
                            <th scope="col" >Hub</th>
                            <th scope="col" width='20%'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($customer as $key => $item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$item->nm_customer}}</td>
                                <td>{{$item->no_telp}}</td>
                                <td>{{$item->jenis_koneksi_customer}}</td>
                                <td>{{number_format($item->harga_sewa)}}</td>
                                <td>{{$item->alamat_customer}}</td>
                                <td>{{$item->nm_hub}}</td>
                                <td>
                                    
                                    <form class="mt-2" action="/customer/{{$item->id}}" method="post">
                                        <a href="/customer/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                                        <a href="/customer/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    @csrf
                                    @method('delete')
                                    <input type="submit" value='Delete' class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <td colspan='8'>Data Tidak Ada</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection