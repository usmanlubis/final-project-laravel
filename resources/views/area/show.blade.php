@extends('layout.master')
@section('title')
@endsection
@section('content')

<form method="post" action='/area'>
    @csrf
    <div class="row">
        <div class="col-xl-12 mx-auto">
            <div class="card border-top border-0 border-4 border-info">
                <div class="card-body">
                    <div class="border p-4 rounded">
                        <div class="card-title d-flex align-items-center">
                            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
                            </div>
                            <h5 class="mb-0 text-info">Area</h5>
                        </div>
                        <hr>
                        <div class="row mb-3">
                            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Nama Area</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name='nm_area' readonly value='{{$area->nm_area}}'>
                                
                            </div>
                            
                        </div>
                        <div class="row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-3">
                                <a href='/area'><button type="button" class="btn btn-info btn-sm">Kembali</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </form>

@endsection