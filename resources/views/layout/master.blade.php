<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
	<link rel="icon" href="assets/images/favicon-32x32.png" type="image/png" />
	<!--plugins-->
	<link href="{{asset('template/assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
	<link href="{{asset('template/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" />
	<link href="{{asset('template/assets/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
	<!-- loader-->
	<link href="{{asset('template/assets/css/pace.min.css')}}" rel="stylesheet" />
	<script src="{{asset('template/assets/js/pace.min.js')}}"></script>
	<!-- Bootstrap CSS -->
	<link href="{{asset('template/assets/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('template/assets/css/bootstrap-extended.css')}}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<link href="{{asset('template/assets/css/app.css')}}" rel="stylesheet">
	<link href="{{asset('template/assets/css/icons.css')}}" rel="stylesheet">
	<!-- Theme Style CSS -->
	<link rel="stylesheet" href="{{asset('template/assets/css/dark-theme.css')}}" />
	<link rel="stylesheet" href="{{asset('template/assets/css/semi-dark.css')}}" />
	<link rel="stylesheet" href="{{asset('template/assets/css/header-colors.css')}}" />
	@stack('style');
	<title>Synadmin – Bootstrap5 Admin Template</title>
</head>

<body>
	<!--wrapper-->
	<div class="wrapper">
		<!--sidebar wrapper -->
		@include('partial.sidebar')
		<!--end sidebar wrapper -->
		<!--start header -->
		@include('partial.header')
		<!--end header -->
		<!-- tambahan alert-->
		@include('sweetalert::alert')
		<!--end tambahan alert -->
		<!--start page wrapper -->
		<div class="page-wrapper">
			<div class="page-content">
				<!--<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Components</div>
				</div>-->

				<div class="card-body">
					@yield('content')
				</div>

			</div>
		</div>
		<!--end page wrapper -->
		<!--start overlay-->
		<div class="overlay toggle-icon"></div>
		<!--end overlay-->
		<!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
		<!--End Back To Top Button-->
		<footer class="page-footer">
			<p class="mb-0">Copyright © 2021. All right reserved.</p>
		</footer>
	</div>
	<!--end wrapper-->
	<!--start switcher-->
	
	<!--end switcher-->
	<!-- Bootstrap JS -->
	<script src="{{asset('template/assets/js/bootstrap.bundle.min.js')}}"></script>
	<!--plugins-->
	<script src="{{asset('template/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('template/assets/plugins/simplebar/js/simplebar.min.js')}}"></script>
	<script src="{{asset('template/assets/plugins/metismenu/js/metisMenu.min.js')}}"></script>
	<script src="{{asset('template/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
	<!--app JS-->
	<script src="{{asset('template/assets/js/app.js')}}"></script>
	@stack('script');
</body>

</html>